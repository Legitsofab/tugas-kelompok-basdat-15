from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, '12.html')

def viewtestimoni(request):
    return render(request, '13.html')

def removetestimoni(request):
    return render(request, '14b.html')

def edittestimoni(request):
    return render(request, '14a.html')

def bayartransaksi(request):
    return render(request, '11.html')
    