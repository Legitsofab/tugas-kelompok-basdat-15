from django import forms
from django.db import connection

class Event(forms.Form):
    choices = [('Sosial','Sosial'),('Fashion','Fashion'),('Olahraga','Olahraga'),('Musik','Musik'),('Seni','Seni')]

    cursor = connection.cursor()
    cursor.execute('set search_path to eventplus')
    cursor.execute('select * from tipe')
    tipe = cursor.fetchall()

    namaEvent = forms.CharField(label='Nama Event',required=True)
    deskripsiEvent = forms.CharField(label='Deskripsi Event',required=True,widget=forms.Textarea)
    temaEvent = forms.ChoiceField(label='Tema Event',required=True,choices=choices,widget=forms.CheckboxSelectMultiple)
    tanggalMulai = forms.DateField(label='Tanggal Mulai',required=True,widget=forms.DateInput(attrs={'type':'date'}))#kurang
    tanggalSelesai = forms.DateField(label='Tanggal Selesai',required=True,widget=forms.DateInput(attrs={'type':'date'}))#kurang
    jamMulai = forms.TimeField(label='Jam Mulai', required=True,widget=forms.TimeInput(attrs={'type':'time'}))#kurang
    jamSelesai = forms.TimeField(label='Jam Selesai', required=True,widget=forms.TimeInput(attrs={'type':'time'}))#kurang
    tipeEvent = forms.ChoiceField(label='Tipe Event',required=True,choices=tipe,widget=forms.Select)

class Lokasi(forms.Form):
    namaGedung = forms.CharField(required=True,widget=forms.TextInput(attrs={'placeholder':'Nama Gedung'}))
    Kota = forms.CharField(required=True,widget=forms.TextInput(attrs={'placeholder':'Nama Kota'}))
    Alamat = forms.CharField(required=True,widget=forms.Textarea(attrs={'placeholder':'Alamat','rows':3}))

class DaftarTiket(forms.Form):
    namaKelas = forms.CharField(required=True,widget=forms.TextInput(attrs={'placeholder':'Nama Kelas Tiket'}))
    Tarif = forms.CharField(required=True,widget=forms.NumberInput(attrs={'placeholder':'Tarif'}))
    Kapasitas = forms.CharField(required=True,widget=forms.NumberInput(attrs={'placeholder':'Kapasitas','rows':3}))