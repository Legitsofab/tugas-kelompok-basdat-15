from django.urls import path
from . import views

app_name = 'kuning'

urlpatterns = [
    path('event/view/all/<int:page>', views.viewOrg1,name='viewOrg1'),
    path('event/create/id', views.eventCreate,name='eventCreate'),
    path('event/update/id', views.eventUpdate,name='eventUpdate'),
    path('event/delete/id', views.eventDel,name='eventDel'),
]
