from django.shortcuts import render
from django.db import connection
from django.core.paginator import Paginator
from .forms import Event,Lokasi,DaftarTiket

# Create your views here.
def viewOrg1(request,page):
    cursor = connection.cursor()
    cursor.execute('select * from eventplus.event')
    eventAll = cursor.fetchall()
    eventPag = Paginator(eventAll,10)
    
    pageObj = eventPag.get_page(page)

    cursor.execute('select * from eventplus.tema')
    temaAll = cursor.fetchall()

    cursor.execute('select * from eventplus.perusahaan')
    perusahaanAll = cursor.fetchall()

    cursor.execute('select * from eventplus.individu')
    individuAll = cursor.fetchall()

    cursor.execute('SELECT lpe.ID_EVENT, l.NAMA_GEDUNG, l.KOTA FROM eventplus.LOKASI_PENYELENGGARAAN_EVENT lpe, eventplus.LOKASI l WHERE l.KODE = lpe.KODE_LOKASI')
    lokasiAll = cursor.fetchall()
    role = ''
    email = ''

    if request.session._session:
        role = request.session['role']
        email = request.session['email']
    # page_number = request.GET.get('page')
    # page_obj = Paginator.get_page(page_number)
    context = {
        'eventAll' : pageObj,
        'temaAll'  : temaAll,
        'perusahaanAll' : perusahaanAll,
        'individuAll' : individuAll,
        'lokasiAll' : lokasiAll,
        'role' : role,
        'email' : email,
    }
    return render(request, 'test.html', context)

def eventCreate(request):
    cursor = connection.cursor()
    # if request.method == 'POST':

    formsEvent = Event()
    formsLokasi = Lokasi()
    formsDaftarTiket = DaftarTiket()
    context = {
        'formsEvent' : formsEvent,
        'formsLokasi': formsLokasi,
        'formsDaftarTiket' : formsDaftarTiket,
    }
    return render(request, 'buatEvent.html', context)

def eventUpdate(request):
    cursor = connection.cursor()
    # if request.method == 'POST':

    formsEvent = Event()
    formsLokasi = Lokasi()
    formsDaftarTiket = DaftarTiket()
    context = {
        'formsEvent' : formsEvent,
        'formsLokasi': formsLokasi,
        'formsDaftarTiket' : formsDaftarTiket,
    }
    return render(request, 'updateEvent.html', context)

def eventDel(request):
    return render(request, 'konfirmasiDelete.html')