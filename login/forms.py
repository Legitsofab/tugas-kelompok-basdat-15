from django import forms

class LoginForm(forms.Form):
    email = forms.EmailField(label='email', required=True)
    password = forms.CharField(label='password', required=True, widget=forms.PasswordInput)

class RegisPengunjungForm(forms.Form):
    email = forms.EmailField(label='Email', required=True)
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput)
    nama_depan = forms.CharField(label='Nama Depan', required=True)
    nama_belakang = forms.CharField(label='Nama Belakang', required=True)
    alamat = forms.CharField(label='Alamat', widget=forms.Textarea(attrs={'rows': 3}))

class RegisOrganizerForm(forms.Form):
    pilihan = [("Individu","Individu"),("Perusahaan","Perusahaan")]

    jenis = forms.ChoiceField(label="jenis", choices=pilihan, widget=forms.RadioSelect)
    email = forms.EmailField(label='Email', required=True)
    password = forms.CharField(label='Password', required=True, widget=forms.PasswordInput)
    npwp_noktp = forms.CharField(label='NPWP/NoKTP', required=True)
    nama = forms.CharField(label='Nama', required=True)

