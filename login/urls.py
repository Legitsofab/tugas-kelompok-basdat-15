from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'login'

urlpatterns = [
    path('', views.welcome, name='welcome'),
    path('login/', views.login, name='login'),
    path('regis_organizer/', views.regis_organizer, name='regis_organizer'),
    path('regis_pengunjung/', views.regis_pengunjung, name='regis_pengunjung'),
    path('logout/', views.logout, name='logout'),
]