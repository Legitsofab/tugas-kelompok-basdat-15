from django.shortcuts import render, redirect
from .forms import LoginForm, RegisOrganizerForm, RegisPengunjungForm
from django.db import connection
from django.contrib import messages


def welcome(request):
	cursor = connection.cursor()
	cursor.execute("set search_path to eventplus;")

	return render(request, 'welcome.html')

def login(request):
	if request.method =='POST':
		form = LoginForm(request.POST)
		email = request.POST.get("email")
		password = request.POST.get("password")

		cursor = connection.cursor()

		cursor.execute("select email, password from eventplus.pengguna where email = '" + email + "';")
		user = cursor.fetchone()
        
		if user != None:
			if password == user[1]:
				request.session['email'] = email
				request.session['role'] = get_role(email)

				if get_role(email) == "organizer":
					return redirect('/kuning/event/view/all/1')
				return redirect('/kuning/event/view/all/1')
		else:
			messages.error(request, 'Email/Password gagal. Silahkan ulangi Kembali dengan informasi yang benar')

	else:
		form = LoginForm()
		context = {'form' : form}
		return render(request, 'login.html', context)


	form = LoginForm()
	context = {'form' : form}

	return render(request, 'login.html', context)

def regis_organizer(request):
	if request.method =='POST':
		form = RegisOrganizerForm(request.POST)

		jenis = request.POST.get("jenis")
		email = request.POST.get("email")
		password = request.POST.get("password")
		npwp_nokpt = request.POST.get("npwp_nokpt")
		nama = request.POST.get("nama")

		cursor = connection.cursor()
		cursor.execute("select count(*) from eventplus.pengguna where email = '" + email + "';")
		jumlah_email = cursor.fetchone()

		if form.is_valid():

			# jenis = dict(form.fields["jenis"].choices)[form.cleaned_data["jenis"]]
			
			# jenis = form.cleaned_data.get("jenis")
			# email = form.cleaned_data.get("email")
			# password = form.cleaned_data.get("password")
			# npwp_nokpt = form.cleaned_data.get("npwp_nokpt")
			# nama = form.cleaned_data.get("nama")


			if password_isvalid(password) == True:
				if jumlah_email[0] == 0:
					try:
						cursor.execute("insert into eventplus.pengguna VALUES ('" + email + "','" + password + "');")
						connection.commit()
						cursor.execute("insert into eventplus.organizer VALUES ('" + email + "','" + npwp_noktp + "');")
						connection.commit()
						
						if jenis == "Individu":
							cursor.execute("insert into eventplus.individu VALUES ('" + email + "'," + npwp_noktp + ",'" + nama + "','" + nama + "');")
							connection.commit()
						else:
							cursor.execute("insert into eventplus.perusahaan VALUES ('" + email + "','" + nama + "');")
							connection.commit()	
						messages.success(request, 'Registrasi Berhasil. Silahkan lakukan Login')
						return redirect('/login/')
					except:
						messages.error(request, 'mohon maaf terdapat masalah pada database')

				else:
					messages.error(request, 'email sudah terdaftar')
			else:
				messages.error(request, 'Password tidak valid, pastikan password Anda minimal mengandung 6 karakter, 1 huruf kapital, 1 angka dan 1 symbol ')
		else:
			messages.error(request, 'data tidak valid')

	else:
		form = RegisOrganizerForm()
		context = {'form' : form}
		return render(request, 'regis_organizer.html', context)

	form = RegisOrganizerForm()
	context = {'form': form}
	return render(request, 'regis_organizer.html', context)

def regis_pengunjung(request):
	if request.method =='POST':
		
		form  = RegisPengunjungForm(request.POST)

		email = request.POST.get("email")
		password = request.POST.get("password")
		nama_depan = request.POST.get("nama_depan")
		nama_belakang = request.POST.get("nama_belakang")
		alamat = request.POST.get("alamat")

		cursor = connection.cursor()
		cursor.execute("select count(*) from eventplus.pengguna where email = '" + email + "';")
		jumlah_email = cursor.fetchone()

		if form.is_valid():
			if password_isvalid(password) == True:
				if jumlah_email[0] == 0:
					try:
						cursor.execute("insert into eventplus.pengguna VALUES ('" + email + "','" + password + "');")
						connection.commit()
						cursor.execute("insert into eventplus.pengunjung VALUES ('" + email + "','" + nama_depan + "','" + nama_belakang + "','" + alamat + "');")
						connection.commit()
						messages.success(request, 'Registrasi Berhasil. Silahkan lakukan Login')
						return redirect('/login/')
					except:
						messages.error(request, 'mohon maaf terdapat masalah pada database')

				else:
					messages.error(request, 'email sudah terdaftar')

			else:
				messages.error(request, 'Password tidak valid, pastikan password Anda minimal mengandung 6 karakter, 1 huruf kapital, 1 angka dan 1 symbol ')
		else:
			messages.error(request, 'data tidak valid')

	else:
		form = RegisPengunjungForm()
		context = {'form' : form}
		return render(request, 'regis_pengunjung.html', context)		

	form = RegisPengunjungForm()
	context = {'form' : form}
	return render(request, 'regis_pengunjung.html', context)

def get_role(email):
	cursor = connection.cursor()
	cursor.execute("select email from eventplus.pengunjung where email = '" +  email + "'")

	if (cursor.fetchone() != None):
		return "pengunjung"
	return "organizer"

def password_isvalid(password):
	any_digit = False
	any_upper = False
	any_symbol = False

	special_characters = "!@#$%^&*()-+?_=,<>/"

	for pw in password:
		if pw.isdigit():
			any_digit = True
	for pw in password:
		if pw.isupper():
			any_upper = True
	for pw in password:
		if pw in special_characters:
			any_symbol = True

	if len(password) >= 6:
		if any_upper==True and any_digit==True and any_symbol==True:
			return True
	return False

def logout(request):
    request.session.flush()
    return redirect("/login/")
