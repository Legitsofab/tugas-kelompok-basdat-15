from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'merah'

urlpatterns = [
    path('', views.pesan_tiket, name='pesan_tiket'),
    path('lihat_transaksi/', views.lihat_transaksi, name='lihat_transaksi'),
    path('update_transaksi/', views.update_transaksi, name='update_transaksi'),
]