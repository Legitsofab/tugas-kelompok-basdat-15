from django.shortcuts import render

def pesan_tiket(request):
	return render(request, 'pesan_tiket.html')

def lihat_transaksi(request):
	return render(request, 'lihat_transaksi.html')

def update_transaksi(request):
	return render(request, 'update_transaksi.html')

