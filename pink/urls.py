from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'pink'

urlpatterns = [
    path('detailevent/', views.detail_event, name='detail_event'),
    path('profileorganizer/', views.profile_organizer, name='profile_organizer'),
    path('sharedprofit/', views.shared_profit, name='shared_profit'),
]
