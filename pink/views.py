from django.shortcuts import render

# Create your views here.
def detail_event(request):
	return render(request, '15.html')

def profile_organizer(request):
	return render(request, '16.html')

def shared_profit(request):
	return render(request, '17.html')